package ull.modelado;

import static org.junit.Assert.assertTrue;
import org.junit.Test;
import ull.modelado.fraccion.Fraccion;

public class TestFracciones
{
	@Test
	public void sumaFracciones()
	{
		Fraccion t_f = new Fraccion(2, 3);
		Fraccion t_ff = new Fraccion(4, 9);
		Fraccion t_aux = new Fraccion(10, 9);
		assertTrue(t_f.suma(t_ff).equals(t_aux));
	}
	@Test
	public void restaFracciones()
	{
		Fraccion t_f = new Fraccion(2, 3);
		Fraccion t_ff = new Fraccion(4, 9);
		Fraccion t_aux = new Fraccion(2, 9);
		assertTrue(t_f.resta(t_ff).equals(t_aux));
	}
	@Test
	public void divisionFracciones()
	{
		Fraccion t_f = new Fraccion(2, 3);
		Fraccion t_ff = new Fraccion(4, 9);
		Fraccion t_aux = new Fraccion(3, 2);
		assertTrue(t_f.division(t_ff).equals(t_aux));
	}
	@Test
	public void multiplicacionFracciones()
	{
		Fraccion t_f = new Fraccion(2, 3);
		Fraccion t_ff = new Fraccion(4, 9);
		Fraccion t_aux = new Fraccion(8, 27);
		assertTrue(t_f.multiplicacion(t_ff).equals(t_aux));
	}
	/*
	public static void main(String[] args)
	{
		Fraccion t_f = new Fraccion(2, 3);
		Fraccion t_ff = new Fraccion(4, 9);
		Fraccion t_aux =t_f.multiplicacion(t_ff);
		System.out.println(t_aux);
	}*/
}
