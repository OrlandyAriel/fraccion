package ull.modelado.fraccion;
/**
 * @author Orlandy Ariel S�nchez A.
 */
public class Fraccion
{ // Atributos
	private int m_numerador, m_denominador;
	// Constructores y funciones
	public Fraccion(int a_numerador, int a_denominador)
	{
		int t_mcd = mcd(a_numerador, a_denominador);
		m_numerador = a_numerador / t_mcd;
		m_denominador = a_denominador / t_mcd;
	}
	@Override
	public String toString()
	{
		return "  " + m_numerador + "\n-----\n  " + m_denominador;
	}
	@Override
	public boolean equals(Object obj)
	{
		Fraccion t_aux = (Fraccion) obj;
		if (m_numerador == t_aux.getM_numerador() && m_denominador == t_aux.getM_denominador())
			return true;
		else
			return false;
	}
	public int mcd(int a_num, int a_den)
	{
		/*
		 * if (a_den == 0) return a_num; else return mcd(a_den, (a_num%a_den));
		 */
		while (a_den != 0)
		{
			int aux = (a_num % a_den);
			a_num = a_den;
			a_den = aux;
		}
		return a_num;
	}
	public Fraccion suma(Fraccion a_fraccion)
	{
		int t_num1, t_num2, t_rest, t_den;
		t_num1 = (this.m_numerador * a_fraccion.getM_denominador());
		t_num2 = (this.m_denominador * a_fraccion.getM_numerador());
		t_rest = t_num1 + t_num2;
		t_den = (this.m_denominador * a_fraccion.getM_denominador());
		// si lo quiero devolver al objeto que llama a la funci�n
		// this.m_numerador = t_rest;
		// this.m_denominador = t_den;
		return new Fraccion(t_rest, t_den);
	}
	public Fraccion resta(Fraccion a_fraccion)
	{
		int t_num1, t_num2, t_rest, t_den;
		t_num1 = (this.m_numerador * a_fraccion.getM_denominador());
		t_num2 = (this.m_denominador * a_fraccion.getM_numerador());
		t_rest = t_num1 - t_num2;
		t_den = (this.m_denominador * a_fraccion.getM_denominador());
		// si lo quiero devolver al objeto que llama a la funci�n
		// this.m_numerador = t_rest;
		// this.m_denominador = t_den;
		return new Fraccion(t_rest, t_den);
	}
	public Fraccion multiplicacion(Fraccion a_fraccion)
	{
		return new Fraccion(m_numerador * a_fraccion.getM_numerador(), m_denominador * a_fraccion.getM_denominador());
	}
	public Fraccion division(Fraccion a_fraccion)
	{
		return new Fraccion(m_numerador * a_fraccion.getM_denominador(), m_denominador * a_fraccion.getM_numerador());
	}
	// Getters & Setters
	public int getM_numerador()
	{
		return m_numerador;
	}
	public void setM_numerador(int m_numerador)
	{
		this.m_numerador = m_numerador;
	}
	public int getM_denominador()
	{
		return m_denominador;
	}
	public void setM_denominador(int m_denominador)
	{
		this.m_denominador = m_denominador;
	}
}